using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    UnityEngine.AI.NavMeshAgent agent;
    public Transform ObjectToFollow;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject != null)
        {
            agent.destination = ObjectToFollow.position;
        }
    }
}
