using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wandering : MonoBehaviour
{
    public GameObject[] Destinations;
    UnityEngine.AI.NavMeshAgent agent;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        ChangeDestination();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject != null)
        {
            if (Random.Range(0, (int)(20.0 / Time.deltaTime)) == 0) //zmena smeru cca 1 za 20 sekund
            {
                ChangeDestination();
            }
        }
    }

    private void ChangeDestination()
    {
        var dst = Destinations[Random.Range(0, Destinations.Length)];
        agent.destination = dst.transform.position;
    }
}
