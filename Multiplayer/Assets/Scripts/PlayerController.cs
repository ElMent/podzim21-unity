using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;

public class PlayerController : MonoBehaviourPun
{
    private CharacterController characterController;
    private InputMaster controls;
    private Vector2 moveInput;
    private float groundDistance = 0.4f;
    private bool isGrounded;
    private Vector3 velocity;
    private float gravity = -9.81f;
    private LineRenderer shootingRay;
    private int health = 5;
    public int MaxHealth = 5;


    public float jumpHeight = 2f;
    public Transform groundCheck;
    public LayerMask groundLayer;
    public Camera camera;

    public float movementSpeed = 1f;
    public float weaponRange = 100f;
    public float weaponRechargeTime = 0.5f;
    private object weaponLock = new object();

    public Transform gunEnd;

    public Text textHealth;
    public Text textTime;

    private DateTime start;
    private bool weaponCharged = true;
    private bool isShooting = false;
        
    private void Awake()
    {
        //propoj se s ovladacem abys mohl cist jeho hodnoty - zmen null
        controls = new InputMaster();
        //najdi komponentu CharacterComponent, se kterou budeme hybat - zmen null
        characterController = GetComponent<CharacterController>();
        shootingRay = GetComponent<LineRenderer>();
        start = DateTime.Now;
        health = MaxHealth;
    }

    private void Start()
    {
        //camera.enabled = photonView.IsMine;
    }

    private void Update()
    {
        if (photonView.IsMine) //vetsinu akci delam pouze pro lokalniho hrace
        {
            Gravity();
            PlayerMovement();
            Jump();
            CheckShooting();
            UpdateTime();
            CheckZombieAttacks();
        }
        RaycastShooting(); //strili i hraci pres sit
    }

    private void CheckZombieAttacks() 
    {
        Collider[] hitColliders = Physics.OverlapCapsule(groundCheck.transform.position, groundCheck.transform.position + new Vector3(0,2,0), 1.5f);        
        foreach(var collider in hitColliders)
        {
            var enemy = collider.gameObject.GetComponent<Enemy>();
            if (enemy != null)
            {
                health -= enemy.GetHit();
                textHealth.text = health.ToString();
                if (health <= 0)
                {
                    BetweenScenesStorage.survivalTime = (int)(DateTime.Now - start).TotalSeconds;
                    SceneManager.LoadScene("GameOverScene");
                }
            }
        }
    }

    private void UpdateTime()
    {
        textTime.text = (DateTime.Now - start).TotalSeconds.ToString("0");
    }

    private void CheckShooting()
    {
        lock (weaponLock)
        {
            if (controls.Player.Shoot.triggered && weaponCharged)
            {
                weaponCharged = false;
                isShooting = true;
                StartCoroutine(WeaponRecharge());
            }
            else
            {
                isShooting = false;
            }
        }
    }

    private void RaycastShooting()
    {
        if (isShooting)
        {            
            StartCoroutine(ShootingEffects());
            Vector3 rayOrigin = camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.0f));
            RaycastHit hit;
            if (Physics.Raycast(rayOrigin, camera.transform.forward, out hit, weaponRange))
            {
                shootingRay.SetPosition(1, hit.point);
                var shootable = hit.collider.GetComponent<Shootable>();
                if (shootable != null)
                {
                    shootable.Damage();
                }

                if (hit.rigidbody != null)
                {
                    hit.rigidbody.AddForce(-hit.normal * 100f);
                }
            }
            else
            {
                shootingRay.SetPosition(1, rayOrigin + (camera.transform.forward * weaponRange));
            }   
        }
    }

    private IEnumerator WeaponRecharge()
    {
        yield return new WaitForSeconds(weaponRechargeTime);
        lock (weaponLock)
        {
            weaponCharged = true;
        }
    }

    private IEnumerator ShootingEffects()
    {
        shootingRay.SetPosition(0, gunEnd.position);
        shootingRay.enabled = true;
        yield return new WaitForSeconds(0.1f);
        shootingRay.enabled = false;
    }

    private void Jump()
    {
        //tento radek spust pouze pokud hrac stiskl klavesu pro skok a postava je na zemi (zm�� false v podm�nce)
        if (isGrounded) { 
            if (controls.Player.Jump.triggered)
            {
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            }
            
        }
    }

    private void Gravity()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundLayer);
        velocity.y += gravity * Time.deltaTime;
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = 0f;
        }
        characterController.Move(velocity * Time.deltaTime);
    }

    private void PlayerMovement()
    {
        //zjisti jak hrac macka klavesy WASD a podle toho nastav tyto promenne:
        var moveInput = controls.Player.Movement.ReadValue<Vector2>();
        float ovladacDopredu = moveInput.y; 
        float ovladacDostran = moveInput.x;
        //
        Vector3 movement = ovladacDopredu * transform.forward + ovladacDostran * transform.right;
        //hrac beha pomalu - uprav kod tak, aby slo movementSpeed zmenit z Unity a nastav ho na lepsi hodnotu
        
        characterController.Move(movement * movementSpeed * Time.deltaTime);
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }
}
