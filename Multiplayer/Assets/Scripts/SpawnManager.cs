using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public Transform ObjectToFollow; //koho maji pronasledovat (pokud maji pronasledovaci skript)
    public GameObject[] Spawns; //seznam mist kde se budou objevovat
    public GameObject Prefab; //sablona pro vytvareni NPC
    public float timeRate = 40.0f;
    
    void Start()
    {
        //startuju proces na pozadi
        StartCoroutine(SpawnNPC());
    }

    private IEnumerator SpawnNPC()
    {
        while (true) //nekonecna smycka - porad pridavam nove NPC
        {
            var spawn = Spawns[Random.Range(0, Spawns.Length)]; //kde se objevi - nahodne vyberu
            //podle sablony v Prefab vytvorim novou kopii NPC na souradnicich podle spawn a otocenou do vychozi pozice
            GameObject npc = Instantiate(Prefab, spawn.transform.position, Quaternion.Euler(0, 0, 0));
            //v hierarchi objektu zaradim pod spawns
            npc.transform.SetParent(transform);
            //pokud ma prefab na sobe skript Follow - vyplnim jeho ObjectToFollow podle nastaveni tohoto skriptu
            var followScript = npc.GetComponent<Follow>();
            if (followScript != null) followScript.ObjectToFollow = ObjectToFollow;
            //cekam dany pocet sekund a postupne zrychluju
            if (timeRate > 1) timeRate -= 0.5f;
            yield return new WaitForSeconds(timeRate);
        }
    }
}
