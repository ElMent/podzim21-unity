using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class GameManager : MonoBehaviourPunCallbacks
{
    public Text textMessages;
    public void OnLeaveButton_Clicked()
    {
        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene("IntroScene"); //melo by byt az v OnLeftRoom
    }

    public override void OnPlayerEnteredRoom(Player other)
    {
        textMessages.text = string.Format("P�ipojil se {0}, celkem hr��� {1}", other.NickName, PhotonNetwork.CurrentRoom.PlayerCount);
    }

    public override void OnPlayerLeftRoom(Player other)
    {
        textMessages.text = string.Format("Ode�el {0}, celkem hr��� {1}", other.NickName, PhotonNetwork.CurrentRoom.PlayerCount);
    }
}
