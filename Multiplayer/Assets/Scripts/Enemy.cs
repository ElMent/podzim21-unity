using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private int energy = 1;
    public int MaxHit = 1;
    public float ReloadTime = 2f;
    public int GetHit() {
        if (energy > 0)
        {
            StartCoroutine(Recharge());
            var hit = energy;
            energy = 0;
            return hit;
        } else return 0;
    }

    IEnumerator Recharge() {
        yield return new WaitForSeconds(ReloadTime);
        energy = MaxHit;
    }
}
