using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Realtime;

public class ButtonHandlers : MonoBehaviourPunCallbacks
{
    const string playerNamePrefKey = "PlayerName";

    public Canvas CanvasForm;
    public Canvas CanvasConnetcting;
    public Text InputRoom;
    public Text InputNick;
    public byte MaxPlayersPerRoom = 10;

    private bool isConnecting = false;

    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    private void Start()
    {
        if (PlayerPrefs.HasKey(playerNamePrefKey))
        {
            InputNick.text = PlayerPrefs.GetString(playerNamePrefKey);
        }
        CanvasForm.enabled = true;
        CanvasConnetcting.enabled = false;
    }
    public void ButtonStart_Clicked() {
        //
        CanvasForm.enabled = false;
        CanvasConnetcting.enabled = true;
        Connect();
    }

    public void Connect()
    {
        // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
        if (PhotonNetwork.IsConnected)
        {
            // #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnJoinRandomFailed() and we'll create one.
            PhotonNetwork.NickName = InputNick.text;
            var roomOptions = new RoomOptions()
            {
                MaxPlayers = MaxPlayersPerRoom, IsVisible = false
            };
            PhotonNetwork.JoinOrCreateRoom(InputRoom.text, roomOptions, TypedLobby.Default);
            PlayerPrefs.SetString(playerNamePrefKey, InputNick.text);
        }
        else
        {
            // #Critical, we must first and foremost connect to Photon Online Server.
            isConnecting = PhotonNetwork.ConnectUsingSettings();
            //PhotonNetwork.GameVersion = gameVersion;
        }
    }

    public override void OnConnectedToMaster()
    {
        if (isConnecting)
        {
            isConnecting = false;
            Connect();
        }
    }

    public override void OnJoinedRoom()
    {
        SceneManager.LoadScene("ZombieSurvival");
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        CanvasForm.enabled = true;
        CanvasConnetcting.enabled = false;
        Debug.Log("PUN room failed: " + message);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        CanvasForm.enabled = true;
        CanvasConnetcting.enabled = false;
        Debug.Log("PUN disconnected. "+cause.ToString());
    }
}
