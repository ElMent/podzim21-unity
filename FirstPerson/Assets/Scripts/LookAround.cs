using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class LookAround : MonoBehaviour
{
    InputMaster controls;
    private float updownRotation = 0f;

    public Transform player;
    public float mouseSensitivity = 80f;

    private void Awake()
    {
        controls = new InputMaster();
        Cursor.lockState = CursorLockMode.Locked;
        player = transform.parent;
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

    private void Update()
    {
        var mouseInput = controls.Player.Look.ReadValue<Vector2>();
        float mouseX = mouseInput.x * mouseSensitivity * Time.deltaTime;
        float mouseY = mouseInput.y * mouseSensitivity * Time.deltaTime;

        //nahoru dolu
        updownRotation -= mouseY;
        transform.localRotation = Quaternion.Euler(updownRotation, 0, 0); //pohled nahoru dolu je okolo osy x

        //otaceni vlevo/vpravo
        player.Rotate(new Vector3(0, mouseX, 0));
    }
}
