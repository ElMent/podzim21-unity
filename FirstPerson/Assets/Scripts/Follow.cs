using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    UnityEngine.AI.NavMeshAgent agent;
    //property kterou nastavim z unity editoru
    public Transform ObjectToFollow;

    void Start()
    {
        //vyhledam komponentu na stejnem objektu jako je skript
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    void Update()
    {
        if (this.gameObject != null)
        {
            agent.destination = ObjectToFollow.position;
        }
    }
}
