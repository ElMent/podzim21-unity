using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowScore : MonoBehaviour
{
    public Text textScore;
    // Start is called before the first frame update
    void Start()
    {
        textScore.text = string.Format("Vydr�el/a jsi {0} sekund.", BetweenScenesStorage.survivalTime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
