using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonHandlers : MonoBehaviour
{
    public void ButtonStart_Clicked() {
        SceneManager.LoadScene("ZombieSurvival");
    }
}
