using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private CharacterController characterController;
    private InputMaster controls;
    private Vector2 moveInput;
    private float groundDistance = 0.4f;
    private bool isGrounded;
    private Vector3 velocity;
    private float gravity = -9.81f;

    public float movementSpeed = 6f;
    public float jumpHeight = 2f;
    public Transform groundCheck;
    public LayerMask groundLayer;

    

    private void Awake()
    {
        controls = new InputMaster();
        characterController = this.GetComponent<CharacterController>();
    }

    private void Update()
    {
        Gravity();
        PlayerMovement();
        Jump();
    }

    private void Jump()
    {
        if (controls.Player.Jump.triggered && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }
    }

    private void Gravity()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundLayer);
        velocity.y += gravity * Time.deltaTime;
        if (isGrounded && velocity.y < 0)
        {
            velocity.y = 0f;
        }
        characterController.Move(velocity * Time.deltaTime);
    }

    private void PlayerMovement()
    {
        moveInput = controls.Player.Movement.ReadValue<Vector2>();
        Vector3 movement = moveInput.y * transform.forward + moveInput.x * transform.right;
        characterController.Move(movement * movementSpeed * Time.deltaTime);
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }
}
