using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class MouseLook : MonoBehaviour
{
    public float mouseSensitivity = 80f;

    private InputMaster controls;
    private Vector2 mouseLook;
    private float updownRotation = 0f; 
    private Transform playerBody;

    private void Awake()
    {
        playerBody = transform.parent;
        controls = new InputMaster();
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        mouseLook = controls.Player.Look.ReadValue<Vector2>();
        float mouseX = mouseLook.x * mouseSensitivity * Time.deltaTime;
        float mouseY = mouseLook.y * mouseSensitivity * Time.deltaTime;

        //pohled nahoru dolu
        updownRotation -= mouseY;
        updownRotation = Mathf.Clamp(updownRotation, -90f, 90f);
        transform.localRotation = Quaternion.Euler(updownRotation, 0, 0); //pohled nahoru dolu je okolo osy x

        //otaceni vlevo/vpravo
        playerBody.Rotate(new Vector3(0, mouseX, 0));
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }

}
