using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTouchPlay : MonoBehaviour
{
    private AudioSource audio;

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !audio.isPlaying)
        {             
            audio.Play();
        }
    }
}
